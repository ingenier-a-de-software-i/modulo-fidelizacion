# Documentaci&oacute;n M&oacute;dulo de Fidelizaci&oacute;n

En el siguiente repositorio se encuentra la documentaci&oacute;n y diagramas del m&oacute;dulo de fidelizaci&oacute;n del proyecto para los alumni promovido por la UEES.

## Estructura del repositorio
```
├── Actas
│   ├── *.pdf
├── {nombre-diagrama}
│   ├── {nombre-diagrama}.png
│   ├── {nombre-diagrama}.xml
├── .gitignore
├── Cronograma.xlsx
├── HistoriasDeUsuario.xlsx
├── README.md
├── gestionDeRiesgos.xlsx
├── historiasDeUsuario.pdf
└── requerimientos.pdf
```

## Importante
Los archivos XML se pueden abrir con la herramienta [draw.io](https://www.drawio.com), arrastr&aacute;ndolos a la interfaz de la aplicaci&oacute;n web.